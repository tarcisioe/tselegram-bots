import { BotAPI, makeAPI } from './api'
import { cast } from './type-utils'
import { ChatId, Message, Token, Update, UpdateId, User } from './types'

type UpdateHandler = (u: Update) => Promise<void>

export class Bot {
    private api: BotAPI
    private lastOffset: UpdateId

    public constructor(
        public base: string,
        public token: Token,
        private handler: UpdateHandler = async (u) => undefined,
    ) {
        this.api = makeAPI(base)
        this.lastOffset = cast<UpdateId>(0)
    }

    public async getMe(): Promise<User>  {
        return this.api.getMe({token: this.token})
    }

    public async getUpdates(offset: UpdateId, timeout: number = 10): Promise<Update[]> {
        return await this.api.getUpdates({
            body: {
                offset,
                timeout,
            },
            token: this.token,
        })
    }

    public async sendText(chatId: ChatId, text: string): Promise<Message> {
        return await this.api.sendMessage({
            body: {
                chat_id: chatId,
                text,
            },
            token: this.token,
        })
    }

    public async longPoll(timeout: number = 10): Promise<void> {
        while (true) {
            const updates = await this.getUpdates(this.lastOffset, timeout)

            for (const update of updates) {
                await this.handler(update)

                this.lastOffset = cast<UpdateId>(
                    cast<number>(update.update_id) + 1,
                )
            }
        }
    }
}

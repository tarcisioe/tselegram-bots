import { requester } from './request'
import { ChatId, Message, Update, UpdateId, User } from './types'

export interface MessageRequest {
    chat_id: ChatId
    text: string
    parse_mode?: 'Markdown' | 'HTML'
    disable_web_page_preview?: boolean
    disable_notification?: boolean
    reply_to_message_id?: number
    reply_markup?: any
}

export type UpdateType = 'message' | 'edited_channel_post' | 'callback_query'

export interface UpdatesRequest {
    offset?: UpdateId
    limit?: number
    timeout?: number
    allowed_updates?: UpdateType[]
}

/* tslint:disable:typedef */
export function makeAPI(base: string) {
    return {
        getMe: requester<User>(
            base, 'GET', 'getMe',
        ),
        getUpdates: requester<Update[], UpdatesRequest>(
            base, 'POST', 'getUpdates',
        ),
        sendMessage: requester<Message, MessageRequest>(
            base, 'POST', 'sendMessage',
        ),
    }
}
/* tslint:enable:typedef */

const apiDummy = false ? makeAPI('') : null
export type BotAPI = typeof apiDummy

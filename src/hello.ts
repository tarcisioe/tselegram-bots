import { Bot } from './bot'
import { cast } from './type-utils'
import { Token } from './types'

const TOKEN = cast<Token>('403837026:AAEZY4Dz76SYg1xN4mzbIbsx2oghm5xqYLM')

async function hello(token: Token): Promise<void> {
    const bot = new Bot(
        'https://api.telegram.org',
        TOKEN,
        async (u) => {
            if (u.message && u.message.text) {
                await bot.sendText(
                    u.message.chat.id,
                    Array.from(u.message.text).reverse().join(''),
                )
            }
        },
    )

    const me = await bot.getMe()

    console.log(me.first_name)

    await bot.longPoll()
}

hello(TOKEN).catch((e) => {
    console.log(e)
    process.exit(-1)
})

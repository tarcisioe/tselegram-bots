import * as querystring from 'querystring'
import { URL } from 'url'

import fetch from 'node-fetch'

import { BotAPIError } from './error'
import { Token } from './types'

export type Method = 'GET' | 'POST'

async function makeAPIRequest<Out, Body, Query>(
    base: string,
    token: Token,
    method: Method,
    resource: string,
    body?: Body,
    queryParameters?: Query,
): Promise<Out> {
    let bodyJson = ''

    if (body) {
        bodyJson = JSON.stringify(body)
    }

    const url = new URL(`${base}`)

    if (queryParameters) {
        url.search = querystring.stringify(queryParameters)
    }

    url.pathname = `bot${token}/${resource}`

    const response = await fetch(url.href, {
        body: bodyJson,
        headers: {
            'Content-Type': 'application/json',
        },
        method,
    })

    const responseBody: BotResponse<Out> = await response.json()

    if (!isOk(responseBody)) {
        throw new BotAPIError(responseBody.description)
    }

    return responseBody.result
}

export const requester = <Out, Body = undefined, Query = undefined>(
    base: string,
    method: Method,
    resource: string,
): (arg: { body?: Body, query?: Query, token: Token }) => Promise<Out> =>
    async (
        arg?,
    ): Promise<Out> => {
        const { body, query, token } = arg

        return await makeAPIRequest<Out, Body, Query>(
            base,
            token,
            method,
            resource,
            body,
            query,
        )
    }

interface SuccessfulBotResponse<T> {
    ok: true
    result: T
}

interface ErrorBotResponse {
    ok: false
    description: string
    error_code: number
}

type BotResponse<T> = SuccessfulBotResponse<T> | ErrorBotResponse

export function isOk<T>(response: BotResponse<T>): response is SuccessfulBotResponse<T> {
    return response.ok
}

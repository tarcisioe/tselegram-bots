export class Token extends String {
    private typed(): void
}

export class ChatId extends Number {
    private typed(): void
}

export class UpdateId extends Number {
    private typed(): void
}

export interface User {
    id: number
    is_bot: boolean
    first_name: string
    last_name?: string
    username?: string
    language_code?: string
}

export interface Chat {
    id: ChatId
}

export interface Message {
    message_id: number
    from: User
    date: number
    chat: Chat
    text?: string
}

export type InlineQuery = any
export type ChosenInlineResult = any
export type CallbackQuery = any
export type ShippingQuery = any
export type PreCheckoutQuery = any

export interface Update {
    update_id: UpdateId
    message?: Message
    edited_message?: Message
    channel_post?: Message
    edited_channel_post?: Message
    inline_query?: InlineQuery
    chosen_inline_result?: ChosenInlineResult
    callback_query?: CallbackQuery
    shipping_query?: ShippingQuery
    pre_checkout_query?: PreCheckoutQuery
}

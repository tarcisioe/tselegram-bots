export class BotAPIError extends Error {
    constructor(message: string) {
        super(message)
        Object.setPrototypeOf(this, new.target.prototype)
    }

    get name(): string {
        return this.constructor.name
    }
}
